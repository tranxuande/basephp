<?php
require_once('base_controller.php');
require_once('models/admin.php');


class AdminController extends BaseController
{
    public $adminModel;

    function __construct()
    {
        $this->folder = 'admin';

        $this->adminModel = new Admin();

        $arr = ['login', 'logout'];

        if (isset($_SESSION['admin']) && $_SESSION['admin']['role_type'] != 1 && isset($_SESSION['action']) && !in_array($_SESSION['action'], $arr)) {
            unset($_SESSION['action']);

            header('location: ../user/userIndex');

            exit;
        }

        if (!isset($_SESSION['admin']) && !in_array($_SESSION['action'], $arr)) {
            header('location: login');
            return;
        }
    }

    public function login()
    {
        if (isset($_SESSION['admin'])) {
            header('location: adminIndex');
            exit();
        }

        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $error = $this->checkInputLogin($_POST);

            if ($email != '' && $password != '') {
                $data = [
                    'email' => $_POST['email'],
                    'password' => ($_POST['password'])
                ];

                $admin = $this->adminModel->findItem($data);

                if ($admin != null) {
                    setSession('admin', $admin);
                    header('location: adminIndex');
                } else {
                    $error['account_not_exist'] = ACCOUNT_NOT_EXIST;
                }
            }

            if (count($error) != 0) {
                $this->render('login', ['error' => $error]);
            }
        }

        $this->render('login');
    }

    public function logout()
    {
        unset($_SESSION['admin']);
        header('location: login');
    }

    public function adminIndex()
    {
        $total_record = count($this->adminModel->getAll());

        $data = $this->pagination($total_record);

        $column = isset($_GET['column']) ? $_GET['column'] : '';


        $sort = 'ASC';

        if (isset($_GET['sort']) && $_GET['sort'] == $sort) {
            $sort = 'DESC';
        }

        $result = $this->adminModel->pagination(['id', 'name', 'email', 'avatar', 'role_type'], $data, $column, $sort);

        $data = [
            'admin' => $result,
            'total_page' => $data['total_page'],
            'current_page' => $data['current_page'],
            'total_record' => $total_record,
            'urlSort' => BASE_URL . 'management/admin/adminIndex?sort=' . $sort . '&page=',
            'url' => BASE_URL . 'management/admin/adminIndex?page='
        ];

        $this->render('adminmana', $data);
    }

    public function create()
    {
        if (isset($_SESSION['img'])) {
            unset($_SESSION['img']);
        }

        if (isset($_SESSION['img_name'])) {
            unset($_SESSION['img_name']);
        }

        $this->render('createadmin');
    }

    public function store()
    {
        $error = $this->checkInputData($_POST);

        $img = $this->uploadImg();

        $errorPass = $this->checkPassword($_POST);

        if ($_POST['password'] == '') {
            $errorPass['password'] = PASSWORD_BLANK;
        }

        if (!empty($errorPass)) {
            $error = array_merge($error, $errorPass);
        }

        if (!is_string($img)) {
            $error = array_merge($error, $img);
        }

        if (count($error) != 0) {
            $this->render('createadmin', ['error' => $error]);
            exit;
        }

        if (empty($error) && is_string($img)) {
            $ad = $this->adminModel->getAll();

            $arr = [];

            foreach ($ad as $item) {
                $arr[] = $item['id'];
            }

            if (!in_array($_POST['id'], $arr)) {
                $data = [
                    'name' => $_POST['name'],
                    'password' => ($_POST['password']),
                    'email' => $_POST['email'],
                    'avatar' => $img,
                    'role_type' => $_POST['role_type'],
                    'ins_id' => $_SESSION['admin']['id'],
                    'ins_datatime' => date("Y-m-d h:i:s"),
                ];

                if ($this->adminModel->create($data)) {
                    $_SESSION['create_success'] = CREATE_SUCCESS;
                    header('location: adminIndex');
                }
            } else {
                echo 'id exist';
                $this->render('createadmin');
                die;
            }
        }
    }

    public function edit()
    {
        if (isset($_SESSION['img'])) {
            unset($_SESSION['img']);
        }

        if (isset($_SESSION['img_update'])) {
            unset($_SESSION['img_update']);
        }

        if (isset($_SESSION['img_name'])) {
            unset($_SESSION['img_name']);
        }

        if (isset($_GET['id'])) {
            $admin = $this->adminModel->findById($_GET['id']);

            if (empty($admin)) {
                header('location: adminIndex');
            }

            $_SESSION['img_update'] = $admin['avatar'];

            if ($admin['del_flag'] == 1) {
                $_SESSION['del_flag'] = ADMIN_DEL_FLAG;

                header('location: adminIndex');

                exit;
            }
            $this->render('updateadmin', ['admin' => $admin]);
        } else {
            header('location: adminIndex');
        }
    }

    public function update()
    {
        if (isset($_GET['id'])) {
            $error = [];

            if ($_POST['name'] == '') {
                $error['name'] = NAME_BLANK;
            }

            if (!$this->validateEmail($_POST['email'])) {
                $error['email_format'] = EMAIL_FORMAT;
            }

            if ($_POST['email'] == '') {
                $error['email_blank'] = EMAIL_BLANK;
            }

            $admin = $this->adminModel->findById($_GET['id']);

            if (empty($admin)) {
                header('location: adminIndex');
            }

            $img = $this->uploadImg();

            if ($_FILES['avatar']['name'] == '') {
                $img = $admin['avatar'];
            }

            if (!is_string($img)) {
                $error = array_merge($error, $img);
            }

            if ($_POST['password'] == '') {
                $password = $admin['password'];
            }

            if ($_POST['password'] != '') {
                $errorPass = $this->checkPassword($_POST);

                $errorPass['password_verify'] = PASSWORD_VERIFY;

                $error = array_merge($error, $errorPass);
            }

            $temp = [
                'admin' => $admin,
                'error' => $error,
            ];

            if (count($error) != 0) {
                $this->render('updateadmin', $temp);
            } else {
                $password = $_POST['password'];

                $data = [
                    'name' => $_POST['name'],
                    'password' => $password,
                    'email' => $_POST['email'],
                    'avatar' => $img,
                    'role_type' => $_POST['role_type'],
                    'upd_id' => $_SESSION['admin']['id'],
                    'udp_datetime' => date("Y-m-d h:i:s"),
                ];

                if ($this->adminModel->updateById($_GET['id'], $data)) {
                    $_SESSION['data_update'] = DATA_UPDATE;
                    header('location: adminIndex');
                }
            }
        } else {
            header('location: adminIndex');
        }
    }

    public function delete()
    {
        if (isset($_GET['id'])) {
            $result = $this->adminModel->findById($_GET['id']);

            if ($result['del_flag'] == 1) {
                $_SESSION['admin_delete'] = ADMIN_DELETE;
                header('location: adminIndex');
            } else {
                $this->adminModel->deleteById($_GET['id']);
                $_SESSION['data_delete'] = DATA_DELETE;
                header('location: adminIndex');
            }
        }
    }

    public function search()
    {
        if (isset($_GET['email']) && isset($_GET['name'])) {
            if ($_GET['email'] == '' && $_GET['name'] == '') {
                $admin = $this->adminModel->getAll(['id', 'name', 'password', 'email', 'avatar', 'role_type']);
            } elseif ($_GET['email'] != '' && $_GET['name'] == '') {
                $data = ['email' => $_GET['email']];

                $admin = $this->adminModel->searchByNameOrEmail($data);
            } elseif ($_GET['name'] != '' && $_GET['email'] == '') {
                $data = ['name' => $_GET['name']];

                $admin = $this->adminModel->searchByNameOrEmail($data);
            } else {
                $admin = $this->adminModel->search($_GET);
            }


            $sort = 'ASC';

            if (isset($_GET['sort']) && $_GET['sort'] == $sort) {
                $sort = 'DESC';
            }

            $total_record = count($admin);

            $data = array_merge($this->pagination($total_record), [
                'email' => $_GET['email'],
                'name' => $_GET['name']
            ]);

            $column = isset($_GET['column']) ? $_GET['column'] : '';

            $result = $this->adminModel->paginationSearch(['id', 'name', 'email', 'avatar', 'role_type'], $data, $column, $sort);

            $data = [
                'admin' => $result,
                'total_page' => $data['total_page'],
                'current_page' => $data['current_page'],
                'total_record' => $total_record,
                'email' => $_GET['email'],
                'name' => $_GET['name'],
                'urlSort' => BASE_URL . 'management/admin/search?sort=' . $sort . '&email=' . $_GET['email'] . '&name=' . $_GET['name'] . '&page=',
                'url' => BASE_URL . 'management/admin/search?email=' . $_GET['email'] . '&name=' . $_GET['name'] . '&page='
            ];

            $this->render('adminmana', $data);
        } else {
            header('location: adminIndex');
        }
    }
}
