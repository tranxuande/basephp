<?php
require_once('base_controller.php');
require_once('models/user.php');

class UserController extends BaseController
{
    public $userModel;

    function __construct()
    {
        $this->folder = 'user';

        $this->userModel = new User();

        $arr = ['login', 'index', 'logout'];

        if (!isset($_SESSION['admin'])  && isset($_SESSION['action']) && !in_array($_SESSION['action'], $arr)) {
            header('location: ../admin/login');
            return;
        }
    }

    function renderUser($file, $data = [])
    {
        $viewFile = 'views/' . $this->folder . '/' . $file . '.php';

        if (isset($viewFile)) {
            extract($data);
            ob_start();

            require_once($viewFile);

            $content = ob_get_clean();

            require_once('views/layouts/layoutUser.php');
        } else {
            header('Location: index.php?controller=pages&action=error');
        }
    }

    public function login()
    {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $error = $this->checkInputLogin($_POST);

        if ($email != '' && $password != '') {
            $data = [
                'email' => $_POST['email'],
                'password' => md5($_POST['password'])
            ];

            $user = $this->userModel->findItem($data);

            if ($user != null) {
                setSession('user', $user);
                header('location: profile');
            } else {
                $error['account_not_exist'] = ACCOUNT_NOT_EXIST;
            }
        }

        if (count($error) != 0) {
            $this->render('login', ['error' => $error]);
        }

        $this->renderUser('login');
    }

    function loginFB()
    {
        if (isset($_SESSION['user'])) {
            header('location: ./index.php/profile');
            exit();
        }

        if (isset($_SESSION['current_user'])) {
            header('location: ./index.php/profile');
            exit();
        }

        require_once 'helper/login_facebook/facebook_source.php';

        $this->render('login', ['loginUrl' => $loginUrl]);
    }

    function logout()
    {
        unset($_SESSION['current_user']);
        unset($_SESSION['user']);
        header('Refresh:0;url=loginFB', true, 303);
    }

    // user

    public function profile()
    {
        $this->renderUser('index');
    }

    public function index()
    {
        require_once 'helper/login_facebook/fb-callback.php';

        $result = $this->userModel->findItemByEmail($fbUser['email']);

        if ($result != null && $result['del_flag'] == 1) {
            unset($_SESSION['current_user']);
            $this->logout();
            exit;
        }

        if (empty($result)) {
            $data = [
                'name' => $fbUser['name'],
                'facebook_id' => $fbUser['id'],
                'email' => $fbUser['email'],
                'avatar' => $fbUser['picture']['url'],
                'ins_datetime' => date("Y-m-d h:i:s"),
            ];

            $result = $this->userModel->create($data);

            $result = $this->userModel->findItem($fbUser['email']);

            if ($result != null) {
                setSession('current_user', $result);
            }
        } else {
            $_SESSION['current_user'] = $result;
        }

        $this->renderUser('index');
    }

    //admin-user
    public function userIndex()
    {
        $total_record = count($this->userModel->getAll());

        $data = $this->pagination($total_record);

        $column = isset($_GET['column']) ? $_GET['column'] : '';

        $sort = 'ASC';

        if (isset($_GET['sort']) && $_GET['sort'] == $sort) {
            $sort = 'DESC';
        }

        $result = $this->userModel->pagination(['id', 'name', 'email', 'facebook_id', 'avatar'], $data, $column, $sort);

        $data = [
            'user' => $result,
            'total_page' => $data['total_page'],
            'current_page' => $data['current_page'],
            'total_record' => $total_record,
            'urlSort' => BASE_URL . 'management/user/userIndex?sort=' . $sort . '&page=',
            'url' => BASE_URL . 'management/user/userIndex?page='
        ];

        $this->render('usermanager', $data);
    }

    public function edit()
    {
        if (isset($_SESSION['img'])) {
            unset($_SESSION['img']);
        }

        if (isset($_SESSION['img_update'])) {
            unset($_SESSION['img_update']);
        }

        if (isset($_SESSION['img_name'])) {
            unset($_SESSION['img_name']);
        }

        if (isset($_GET['id'])) {
            $user = $this->userModel->findById($_GET['id']);

            if (empty($user)) {
                header('location: userIndex');
            }

            $_SESSION['img_update'] = $user['avatar'];

            if ($user['del_flag'] == 1) {
                $_SESSION['del_flag'] = 'User has been deleted';

                header('location: userIndex');

                exit;
            }

            $this->render('updateuser', ['user' => $user]);
            exit;
        } else {
            header('location: userIndex');
        }
    }

    public function update()
    {
        if (isset($_GET['id'])) {
            $error = $this->checkInputData($_POST);

            $user = $this->userModel->findById($_GET['id']);

            if (empty($user)) {
                header('location: userIndex');
            }

            $img = $this->uploadImg();

            if ($_FILES['avatar']['name'] == '') {
                $img = $user['avatar'];
            }

            if (!is_string($img)) {
                $error = array_merge($error, $img);
            }

            if ($_POST['password'] == '') {
                $password = $user['password'];
            }

            if ($_POST['password'] != '') {
                $errorPass = $this->checkPassword($_POST);

                $error = array_merge($error, $errorPass);
            }

            $temp = [
                'user' => $user,
                'error' => $error,

            ];

            if (count($error) != 0) {
                $this->render('updateuser', $temp);
            } else {
                $password = $_POST['password'];

                $data = [
                    'name' => $_POST['name'],
                    'password' => $password,
                    'email' => $_POST['email'],
                    'avatar' => $img,
                    'upd_id' => $_SESSION['admin']['id'],
                    'upd_datetime' => date("Y-m-d h:i:s")
                ];

                if ($this->userModel->updateById($_GET['id'], $data)) {
                    $_SESSION['data_update'] = DATA_UPDATE;

                    header('location: userIndex');
                }
            }
        } else {
            header('location: userIndex');
        }
    }

    public function delete()
    {
        $result = $this->userModel->findById($_GET['id']);

        if ($result['del_flag'] == 1) {
            echo USER_DELETE;
            $this->userIndex();
        } else {
            $this->userModel->deleteById($_GET['id']);
            echo DATA_DELETE;
            $this->userIndex();
        }
    }

    public function search()
    {
        if (isset($_GET['email']) && isset($_GET['name'])) {

            if ($_GET['email'] == '' && $_GET['name'] == '') {
                $user = $this->userModel->getAll(['id', 'name', 'email', 'facebook_id', 'avatar']);
            } elseif ($_GET['email'] != '') {
                $data = ['email' => $_GET['email']];

                $user = $this->userModel->searchByNameOrEmail($data);
            } elseif ($_GET['name'] != '') {
                $data = ['name' => $_GET['name']];

                $user = $this->userModel->searchByNameOrEmail($data);
            } else {
                $user = $this->userModel->search($_GET);
            }

            $sort = 'ASC';

            if (isset($_GET['sort']) && $_GET['sort'] == $sort) {
                $sort = 'DESC';
            }

            $total_record = count($user);

            $data = array_merge($this->pagination($total_record), [
                'email' => $_GET['email'],
                'name' => $_GET['name']
            ]);

            $column = isset($_GET['column']) ? $_GET['column'] : '';

            $result = $this->userModel->paginationSearch(['id', 'name', 'email', 'facebook_id', 'avatar',], $data, $column, $sort);

            $data = [
                'user' => $result,
                'total_page' => $data['total_page'],
                'current_page' => $data['current_page'],
                'total_record' => $total_record,
                'email' => $data['email'],
                'name' => $data['name'],
                'urlSort' => BASE_URL . 'management/user/search?sort=' . $sort . '&email=' . $_GET['email'] . '&name=' . $_GET['name'] . '&page=',
                'url' => BASE_URL . 'management/user/search?email=' . $_GET['email'] . '&name=' . $_GET['name'] . '&page='
            ];

            $this->render('usermanager', $data);
        } else {
            header('location: userIndex');
        }
    }
}
