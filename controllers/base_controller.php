<?php
class BaseController
{
    protected $folder;

    function render($file, $data = [])
    {
        $viewFile = 'views/' . $this->folder . '/' . $file . '.php';

        if (isset($viewFile)) {
            extract($data);
            ob_start();

            require_once($viewFile);

            $content = ob_get_clean();

            require_once('views/layouts/application.php');
        } else {
            header('Location: index.php?controller=pages&action=error');
        }
    }

    public function uploadImg()
    {
        $error = [];
        $flag = true;

        $target_dir = "assets/images/";

        if (isset($_SESSION['img_name'])) {
            $_FILES['avatar']['name'] = $_SESSION['img_name'];
            unset($_SESSION['img_name']);
        }

        $target_file = $target_dir . basename($_FILES['avatar']['name']);
        $imgFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        if (($_FILES['avatar']['name']) == '') {
            $error['file_blank'] = FILE_BLANK;
            $flag = false;
        }

        if ($_FILES['avatar']['error'] != 0) {
            $error['file_error'] = FILE_ERROR;
            $flag = false;
        }

        $allowType = ['jpg', 'png', 'jpeg', 'gif'];

        if (!in_array($imgFileType, $allowType)) {
            $error['file_format'] = FILE_FORMAT;
            $flag = false;
        }

        if ($flag) {
            move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file);
            $_SESSION['img'] = $target_file;
            $_SESSION['img_name'] = $_FILES['avatar']['name'];
            return $target_file;
        }

        // tra ve array neu co loi
        return $error;
    }

    public function checkInputLogin($data)
    {
        $error = [];

        if ($data['email'] == '') {
            $error['email_blank'] = EMAIL_BLANK;
        }

        if ($data['password'] == '') {
            $error['password_blank'] = PASSWORD_BLANK;
        }

        if (count($error) != 0) {
            return $error;
        }

        return [];
    }

    public function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function validateEmail($email)
    {
        $email = $this->test_input($email);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }

    public function validatePassword($password)
    {
        $password = $this->test_input($password);

        if (!preg_match("/\w{8,30}/", $password)) {
            return false;
        }

        return true;
    }


    public function checkInputData($data)
    {
        $error = [];

        if ($data['name'] == '') {
            $error['name'] = NAME_BLANK;
        }

        if (!$this->validateEmail($data['email'])) {
            $error['email_format'] = EMAIL_FORMAT;
        }

        if ($data['email'] == '') {
            $error['email_blank'] = EMAIL_BLANK;
        }

        if (count($error) != 0) {
            return $error;
        }
        return [];
    }


    public function checkPassword($data)
    {
        $error = [];

        if (!$this->validatePassword($data['password'])) {
            $error['password_format'] = PASSWORD_FORMAT;
        }

        if ($data['password'] != $data['password_verify']) {
            $error['password_verify'] = PASSWORD_VERIFY;
        }

        if (count($error) != 0) {
            return $error;
        }

        return [];
    }

    public function pagination($total_record)
    {
        $item_per_page = ITEM_PER_PAGE;

        $current_page = isset($_GET['page']) ? $_GET['page'] : 1;

        $offset = ($current_page - 1) * $item_per_page;

        $total_page = ceil($total_record / $item_per_page);

        $page = [
            'item_per_page' => $item_per_page,
            'offset' => $offset,
            'total_page' => $total_page,
            'current_page' => $current_page,
        ];

        return $page;
    }
}
