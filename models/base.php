<?php
require_once('models/interface/query_interface.php');

abstract class Base implements QueryAdminInterface
{
    public $tableName;
    public $fillAble = [];

    public function getAll($field = [])
    {
        if (empty($field)) {
            $field[] = 'id';
        }

        $db = DB::getinstance();

        $data = implode(",", $field);

        $result = $db->query("SELECT $data FROM $this->tableName WHERE del_flag = " . DELETE_OFF);

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    function findItem($data)
    {
        $db = DB::getinstance();

        $req = $db->prepare(" SELECT * FROM $this->tableName WHERE email = ? AND password = ? AND del_flag =  " . DELETE_OFF);
        $req->execute([$data['email'], $data['password']]);
        $item = $req->fetch(PDO::FETCH_ASSOC);

        if (!empty($item)) {
            return $item;
        }

        return null;
    }

    function findById($id)
    {
        $db = DB::getinstance();

        $sql = $db->prepare(" SELECT * FROM $this->tableName WHERE id = ? ");
        $sql->execute([$id]);
        $item = $sql->fetch(PDO::FETCH_ASSOC);

        if (!empty($item)) {
            return $item;
        }

        return null;
    }


    function deleteById($id)
    {
        $data = [
            'del_flag' => DELETE_ON
        ];

        $this->updateById($id, $data);
    }

    function create($data)
    {
        $db = DB::getinstance();

        $key = implode(',', array_keys($data));

        $this->checkFillAble(array_keys($data));

        $value = implode("','", array_values($data));

        $query = "INSERT INTO $this->tableName ({$key}) VALUES ('{$value}')";
        $sql = $db->prepare($query);
        $sql->execute();

        return true;
    }

    function updateById($id, $data)
    {
        $set = [];

        foreach ($data as $key => $value) {
            $set[] = $key . '=' . "'" . $value . "'";
        }

        $result = implode(',', $set);

        $this->checkFillAble(array_keys($data));

        $db = DB::getinstance();

        $req = $db->prepare(" UPDATE $this->tableName SET " . $result . "WHERE id = ?");
        $req->execute([$id]);

        return true;
    }

    function search($data = [])
    {
        $db = DB::getinstance();

        $sql = $db->query(" SELECT * FROM $this->tableName WHERE email LIKE '%{$data['email']}%' AND name like '%{$data['name']}%' AND del_flag = " . DELETE_OFF);

        return $sql->fetchAll();
    }

    public function checkFillAble($data)
    {
        foreach ($data as  $value) {
            if (!in_array($value, $this->fillAble)) {
                echo $value . ' not exist';
                exit;
            }
        }
    }

    function searchByNameOrEmail($data)
    {
        $db = DB::getinstance();

        foreach ($data as $key => $value) {
            $str = $key . ' like ' . "'%" . $value . "%'";
        }

        $sql = $db->prepare(" SELECT * FROM $this->tableName WHERE $str  AND del_flag = " . DELETE_OFF);
        $sql->execute();
        $item = $sql->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($item)) {
            return $item;
        }

        return [];
    }

    function pagination($field = [], $page, $column, $sort)
    {
        if (empty($field)) {
            $field[] = 'id';
        }

        $db = DB::getinstance();

        $data = implode(",", $field);


        if ($column != '') {
            $result = $db->query("SELECT $data FROM $this->tableName " . " WHERE del_flag = " . DELETE_OFF . " ORDER BY `{$column}` {$sort} LIMIT {$page['item_per_page']} OFFSET {$page['offset']}");
        } else {
            $result = $db->query("SELECT $data 
            FROM $this->tableName " .
                " WHERE del_flag = " . DELETE_OFF .
                " LIMIT {$page['item_per_page']} OFFSET {$page['offset']} ");
        }

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    function paginationSearch($field = [], $array, $column, $sort)
    {
        if (empty($field)) {
            $field[] = 'id';
        }

        $db = DB::getinstance();

        $data = implode(",", $field);

        if ($column != '') {
            $result = $db->query("SELECT $data FROM $this->tableName WHERE email LIKE '%{$array['email']}%' AND name LIKE'%{$array['name']}%' AND del_flag = " . DELETE_OFF . "   ORDER BY {$column} {$sort} LIMIT {$array['item_per_page']} OFFSET {$array['offset']} ");
        } else {
            $result = $db->query("SELECT $data FROM $this->tableName WHERE email LIKE '%{$array['email']}%' and name LIKE'%{$array['name']}%' AND del_flag = " . DELETE_OFF . " LIMIT {$array['item_per_page']} OFFSET {$array['offset']} ");
        }

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
}
