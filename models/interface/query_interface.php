<?php

interface QueryAdminInterface
{
    function getAll($field);    

    function findById($id);

    function deleteById($id);

    function create($data);

    function updateById($id, $data);

    function search($data);
}
