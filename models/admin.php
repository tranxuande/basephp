<?php
require_once('models/base.php');
class Admin extends Base
{
    public $fillAble = ['id', 'name', 'password', 'email', 'avatar', 'role_type', 'ins_id', 'upd_id', 'ins_datatime', 'udp_datetime', 'del_flag'];

    function __construct()
    {
        $this->tableName = 'admin';
    }

}
