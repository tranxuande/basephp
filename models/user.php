<?php

require_once('models/base.php');
class User extends Base
{
    public $fillAble = ['id', 'name', 'facebook_id', 'email', 'avatar', 'role_type', 'ins_id', 'upd_id', 'ins_datetime', 'upd_datetime', 'del_flag', 'password'];

    function __construct()
    {
        $this->tableName = 'user';
    }

    function findItemByEmail($email)
    {
        $db = DB::getinstance();

        $req = $db->prepare(" SELECT * FROM $this->tableName WHERE email = ? ");
        $req->execute([$email]);
        $item = $req->fetch(PDO::FETCH_ASSOC);

        if (!empty($item)) {
            return $item;
        }

        return null;
    }
    
}
