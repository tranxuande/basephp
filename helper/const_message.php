<?php

define('CREATE_SUCCESS', 'Insert data successful');

define('DATA_NOT_FOUND', 'Data not found!');

define('DATA_UPDATE', 'Update data successful');

define('ACCOUNT_NOT_EXIST', 'Account not exist');

define("DATA_DELETE", 'Delete success');

define('NAME_BLANK', 'name not blank');

define('EMAIL_BLANK', 'email not blank');

define('EMAIL_FORMAT', 'Invalid email format');

define('PASSWORD_BLANK', 'password not blank');

define('PASSWORD_FORMAT', 'Length password from 8 to 30');

define('PASSWORD_VERIFY', 'password not the same');

define('FILE_EXIST', 'file exist');

define('FILE_BLANK', 'file blank');

define('FILE_ERROR', 'file error');

define('FILE_FORMAT', 'File is not in the correct format');

define('ADMIN_DEL_FLAG', 'Admin has been revove, can not update');

define('RETURN_DATA', "Data input error, returned old data !!!");

define('ADMIN_DELETE', "Admin has been removed !!!");

define('USER_DELETE', "User has been removed !!!");

define('CAN_NOT_ACCESS', "<script>
                            alert('You can not access!')
                        </script>");
