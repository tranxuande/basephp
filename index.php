<?php

session_start();

date_default_timezone_set("Asia/Ho_Chi_Minh");

require_once 'helper/const_message.php';
require_once("connection.php");
require_once('helper/common.php');
require_once('helper/const_config.php');

$url = $_SERVER['REQUEST_URI'];

$controlActionString = explode('?', $url)[0];

$controlActionArray = explode("/", $controlActionString);
//print_r($controlActionArray);die;

if (isset($controlActionArray[3]) && isset($controlActionArray[4]) && isset($controlActionArray[5]) && $controlActionArray[3] == 'management') {
    $controller = $controlActionArray[4];

    $_SESSION['controller'] = $controller;

    $action = $controlActionArray[5];

    $_SESSION['action'] = $action;
} 
elseif (isset($controlActionArray[3])) {
    $controller = 'user';

    $action = $controlActionArray[3];
} else {
    $controller = 'user';
    
    $action = 'loginFB';
}

require_once('routers.php');
