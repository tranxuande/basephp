<?php
$controllers = array(
    'pages' => ['home', 'error'],
    'admin' => ['login', 'index', 'create', 'store', 'edit', 'update', 'delete', 'search', 'logout', 'adminIndex'],
    'user' => ['login','loginFB','profile','logout','index','userIndex','edit','update','delete','search']
);

if (!array_key_exists($controller, $controllers) || !in_array($action, $controllers[$controller])) {
    $controller = 'pages';
    $action = 'error';
}

include_once('controllers/' . $controller . '_controller.php');

$klass = str_replace('_', '', ucwords($controller, '_')) . 'Controller';
$controller = new $klass;
$controller->$action();
