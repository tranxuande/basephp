<link rel="stylesheet" href="assets/css/admin/login.css" />
<div id="container">
    <p>Login Admin</p>
    <form method="post">
        <div>
            <label>Email</label><br />
            <input name="email" type="email"><br />
            <?php echo isset($error['email_blank']) ? "<div class='input'>" . $error['email_blank'] . "</div/>" : '' ?>
        </div>
        <div>
            <label>Password</label><br />
            <input type="password" name="password"><br />
            <?php echo isset($error['password_blank']) ? "<div class='input'>" . $error['password_blank'] . "</div/>" : '' ?>
        </div>

        <?php if (isset($error['account_not_exist'])) echo  "<div class='input'>" . $error['account_not_exist'] . "</div/>" ?>
        <button type="submit" name="submit">LogIn</button>
    </form>
</div>