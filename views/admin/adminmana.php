<link rel="stylesheet" href="assets/css/admin/mana.css" />
<style>
    #form {
        width: 95%;
    }

    table {
        width: 95%;
        margin: 0 auto;
    }



    input {
        width: 95%;
        height: 30px;
        margin: 10px auto;
    }

    .notification {
        color: white;
    }

    #reset{
        margin-left: 30px;
        padding: 10px 20px;
        background-color: burlywood;
        border-radius: 10px;
    }
    #reset:hover{
        text-decoration: none;
    }
</style>

<?php
if (isset($_SESSION['create_success'])) {
    echo "<p class='notification'>{$_SESSION['create_success']}</p>";
}
unset($_SESSION['create_success']);
?>

<?php
if (isset($_SESSION['del_flag'])) {
    echo "<p class='notification'>{$_SESSION['del_flag']}</p>";
}
unset($_SESSION['del_flag']);
?>

<?php
if (isset($_SESSION['admin_delete'])) {
    echo "<p class='notification'>{$_SESSION['admin_delete']}</p>";
}
unset($_SESSION['admin_delete']);
?>

<?php
if (isset($_SESSION['data_delete'])) {
    echo "<p class='notification'>{$_SESSION['data_delete']}</p>";
}
unset($_SESSION['data_delete']);
?>

<?php
if (isset($_SESSION['data_update'])) {
    echo "<p class='notification'>{$_SESSION['data_update']}</p>";
}
unset($_SESSION['data_update']);
?>

<div>
    <div id="form">
        <form action='<?php echo BASE_URL ?>management/admin/search' method="GET">
            <div class="input">
                <label>Email</label><br />
                <input name="email" value="<?php if (isset($_GET['email'])) echo $_GET['email'] ?>" />
            </div>
            <div class="input">
                <label>Name</label><br />
                <input name="name" value="<?php if (isset($_GET['name'])) echo $_GET['name'] ?>" />
            </div>
            <button type="submit">Search</button>
            <a href="<?php echo BASE_URL ?>management/admin/adminIndex" id="reset">Reset</a>
        </form>
    </div>
</div>
<div>
    <div class="table">
        <table>
            <thead>
                <?php
                if (count($admin) > 0) {
                    $keys = array_keys($admin[0]);
                    foreach ($keys as $key) {
                ?>

                        <th class='cap'>
                            <a href="<?php echo $urlSort ?><?php echo $current_page ?>&column=<?php echo $key  ?>">
                                <?php echo $key;
                                if ($key != 'avatar') {
                                ?>
                                    <i class="fa fa-sort" aria-hidden="true"></i>
                            </a>
                        </th>
                    <?php } ?>

                <?php } ?>
            </thead>


            <tbody>
                <?php foreach ($admin as $item) { ?>
                    <tr>
                        <td><?php echo $item['id'] ?></td>
                        <td><?php echo $item['name'] ?></td>
                        <td><?php echo $item['email'] ?></td>
                        <td><?php echo "<img src= '{$item['avatar']}'/>" ?></td>
                        <td><?php echo $item['role_type'] == 1 ? 'Super admin' : 'Admin' ?></td>
                        <td><a href="<?php echo BASE_URL ?>management/admin/edit?id=<?php echo $item['id'] ?>">Update</a></td>
                        <td><a href="<?php echo BASE_URL ?>management/admin/delete?id=<?php echo $item['id'] ?>" onclick="return confirm('Are you sure delete id = <?php echo $item['id']  ?>')">Delete</a></td>
                    </tr>
            <?php }
                } else {
                    echo "<p class='notification'>There are no record</p>";
                } ?>
            </tbody>
        </table>

        <div>
            <?php require_once 'views/layouts/pagination.php'; ?>
        </div>
    </div>
</div>