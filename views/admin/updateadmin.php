<link rel="stylesheet" href="assets/css/admin/style.css" />
<style>
    .notification {
        color: white;
    }
</style>
<div class="container">
    <h2>Update admin</h2>
    <form action="<?php echo BASE_URL ?>management/admin/update?id=<?php echo $admin['id'] ?>" method="POST" enctype="multipart/form-data">
        <div>
            <label>Name</label><br />
            <input class="in" name="name" value="<?php
                                                    if (isset($_POST['name'])) echo $_POST['name'];
                                                    else
                                                        echo  $admin['name']; ?>" />
            <?php echo isset($error['name']) ? "<div class='input'>" . $error['name'] . "</div>" : '' ?>
        </div>
        <div>
            <label>Password</label><br />
            <input class="in" name="password" type="password" value="<?php if (isset($_POST['password'])) echo $_POST['password'] ?>" />
            <?php echo isset($error['password_format']) ? "<div class='input'>" . $error['password_format'] . "</div>" : '' ?>
        </div>
        <div>
            <label>Password verify</label><br />
            <input name="password_verify" type='password' class="in" />
            <?php if (isset($error['password_verify'])) {
                echo "<div class='input'>" . $error['password_verify'] . "</div>";
            } ?>
        </div>
        <div>
            <label>email</label><br />
            <input class="in" name="email" value="<?php if (isset($_POST['email'])) echo $_POST['email'];
                                                    else echo isset($admin) ? $admin['email'] : '' ?>" />
            <?php
            if (isset($error['email_blank'])) {
                echo  "<div class='input'>" . $error['email_blank'] . "</div>";
            } elseif (isset($error['email_format'])) {
                echo "<div class='input'>" . $error['email_format'] . "</div>";
            }
            ?>
        </div>
        <div>
            <label>Avatar</label><br />
            <input name="avatar" type="file" onchange="loadFile(event)" /><br />
            <img src="<?php if ((isset($_SESSION['img']))) {
                            echo  $_SESSION['img'];
                        } elseif (isset($_SESSION['img_update'])) {
                            echo $_SESSION['img_update'];
                        }

                        ?>" height="100px" width="100px" id="output" />
            <?php
            if (isset($error['file_format'])) {
                echo "<div class='input'>" . $error['file_format'] . "</div>";
            }
            ?>
        </div>
        <div>
            <label>Role type</label><br />
            <select name="role_type">
                <option value="1" <?php if (isset($_POST['role_type']) && $_POST['role_type'] == 1) echo 'selected';
                                    elseif ( $admin['role_type'] == 1)  echo 'selected';
                                    
                                    ?>>Super admin</option>
                <option value="2" <?php if (isset($_POST['role_type']) && $_POST['role_type'] == 2) echo 'selected';
                                    elseif ( $admin['role_type'] == 2)  echo 'selected';

                                    ?>>Admin</option>
            </select>
        </div>
        <button type="submit">Update</button>
    </form>
</div>
<a href="<?php echo BASE_URL ?>management/admin/adminIndex">Back</a>
<script>
    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src)
        }
    };
</script>