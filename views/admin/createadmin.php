<link rel="stylesheet" href="assets/css/admin/style.css" />
<div class="container">
    <h2>Create Admin</h2>
    <form action="<?php echo BASE_URL ?>management/admin/store" method="POST" enctype="multipart/form-data">
        <div>
            <label>NAME</label><br />
            <input name="name" class="in" value="<?php if (isset($_POST['name'])) echo $_POST['name'] ?>" />
            <?php echo isset($error['name']) ? "<div class='input'>" . $error['name'] . "</div>" : '' ?>
        </div>
        <div>
            <label>PASSWORD</label><br />
            <input name="password" type='password' class="in" value="<?php if (isset($_POST['password'])) echo $_POST['password'] ?>" />
            <?php if (isset($error['password'])) {
                echo "<div class='input'>" . $error['password'] . "</div>";
            } elseif (isset($error['password_format'])) {
                echo "<div class='input'>" . $error['password_format'] . "</div>";
            } ?>
        </div>
        <div>
            <label>PASSWORD VERIFY</label><br />
            <input name="password_verify" type='password' class="in" />
            <?php if (isset($error['password_verify'])) {
                echo "<div class='input'>" . $error['password_verify'] . "</div>";
            } ?>
        </div>
        <div>
            <label>EMAIL</label><br />
            <input name="email" class="in" value="<?php if (isset($_POST['email'])) echo $_POST['email'] ?>" />
            <?php
            if (isset($error['email_blank'])) {
                echo  "<div class='input'>" . $error['email_blank'] . "</div>";
            } elseif (isset($error['email_format'])) {
                echo "<div class='input'>" . $error['email_format'] . "</div>";
            }
            ?>
        </div>
        <div>
            <label>AVATAR</label><br />
            <input name="avatar" type="file" onchange="loadFile(event)" /><br />
            <img style="width: 200px;" id="output" src="<?php
                                                        if (isset($_SESSION['img'])) echo $_SESSION['img'];
                                                            
                                                        ?>" />
            <?php
            if (isset($error['file'])) {
                echo "<div class='input'>" . $error['file'] . "</div>";
            } elseif (isset($error['file_blank'])) {
                echo "<div class='input'>" . $error['file_blank'] . "</div>";
            } elseif (isset($error['file_format'])) {
                echo "<div class='input'>" . $error['file_format'] . "</div>";
            }
            ?>
        </div>
        <div>
            <label>ROLE TYPE</label><br />
            <select name="role_type">
                <option value="1" <?php if (isset($_POST['role_type']) && $_POST['role_type'] == 1) echo 'selected' ?>>Super admin</option>
                <option value="2" <?php if (isset($_POST['role_type']) && $_POST['role_type'] == 2) echo 'selected' ?>>Admin</option>
            </select>
        </div>
        <button type="submit">Create</button>
    </form>
</div>
<a style="color: white;" href="<?php echo BASE_URL ?>management/admin/adminIndex">Back</a>

<script>
    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src)
        }
    };
</script>