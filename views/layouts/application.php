<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />


    <style>
        * {
            margin: 0;
            padding: 0;
        }

        a {
            color: white;
            text-decoration: none;
        }

        .menu {
            background-color: #008dde;
            height: 50px;
        }

        .root>li {
            list-style: none;
            float: left;
            position: relative;
        }

        .root>li>a {
            text-decoration: none;
            color: white;
            padding: 0px 15px 0 15px;
            line-height: 50px;
            display: block;
        }

        .root>li>a:hover {
            text-decoration: underline;
        }

        .submenu {
            width: 150px;
            background-color: #008dde;
            min-width: 100px;
            display: none;
            position: absolute;
        }

        .submenu li {
            list-style: none;
        }

        .submenu li a {
            text-decoration: none;
            display: block;
            color: white;
            line-height: 50px;
        }

        .root>li:hover .submenu {
            display: block;
        }

        .submenu li a:hover {
            text-decoration: underline;
        }

        .active {
            background-color: chocolate;
        }

        .pagging a {
            margin-right: 30px;

        }

        .pagging {
            text-align: center;
        }
    </style>
</head>

<body>
    <base href=" http://localhost/basephp/" />
    <?php if ($file != 'login') { ?>

        <div class="menu">
            <ul class="root">
                <?php if ($_SESSION['admin']['role_type'] == 1) { ?>
                    <li <?php if (isset($_SESSION['controller']) && $_SESSION['controller'] == 'admin')  echo "class='active'" ?>><a href="<?php echo BASE_URL ?>management/admin/adminIndex">Admin management</a>
                        <ul class="submenu">
                            <li><a href="<?php echo BASE_URL ?>management/admin/create">Create</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <li <?php if (isset($_SESSION['controller']) && $_SESSION['controller'] == 'user')  echo "class='active'" ?>><a href="<?php echo BASE_URL ?>management/user/userIndex">User management</a></li>
                <li><a href="<?php echo BASE_URL ?>management/admin/logout">Logout</a></li>
            </ul>
        </div>






        <?= @$content ?>


    <?php } else { ?>
        <?= @$content ?>
    <?php } ?>



</body>

</html>