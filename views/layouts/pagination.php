<nav class="pagging">
    <?php if ($total_record >= 1) { ?>

        <?php if ($current_page > 1) {

            $pre_page = $current_page - 1;
        ?>

            <a href="<?php echo $url . $pre_page ?><?php echo  isset($_GET['column']) ? '&column=' . $_GET['column'] : ''; ?><?php echo isset($_GET['sort']) ? '&sort=' . $_GET['sort'] : '' ?>">
                << </a>

                <?php } ?>
                <?php

                for ($i = 1; $i <= $total_page; $i++) { ?>

                    <?php if ($i != $current_page) {

                        if ($i > $current_page - 2 && $i < $current_page + 2) { ?>

                            <a href="<?php echo $url . $i ?><?php echo  isset($_GET['column']) ? '&column=' . $_GET['column'] : ''; ?><?php echo isset($_GET['sort']) ? '&sort=' . $_GET['sort'] : '' ?>"><?= $i ?></a>

                        <?php }
                    } else { ?>

                        <a style="text-decoration: none ; color:antiquewhite; box-shadow: 0 3px tomato"><?= $i ?></a>

                    <?php } ?>

                <?php }

                ?>
                <?php if ($current_page < $total_page) {
                    $next_page = $current_page + 1;
                ?>
                    <a href="<?php echo $url . $next_page ?><?php echo  isset($_GET['column']) ? '&column=' . $_GET['column'] : ''; ?><?php echo isset($_GET['sort']) ? '&sort=' . $_GET['sort'] : '' ?>">
                        >> </a>
            <?php }
            } ?>
</nav>