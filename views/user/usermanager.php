<link rel="stylesheet" href="assets/css/admin/mana.css" />
<style>
    #form {
        width: 95%;
    }

    table {
        width: 100%;
    }

    input {
        width: 95%;
        height: 30px;
        margin: 10px auto;
    }

    #reset {
        margin-left: 30px;
        padding: 10px 20px;
        background-color: burlywood;
        border-radius: 10px;
    }

    #reset:hover {
        text-decoration: none;
    }
</style>
<?php
if (isset($_SESSION['data_update'])) {
    echo "<p class='notification'>{$_SESSION['data_update']}</p>";
}
unset($_SESSION['data_update']);
?>
<div>
    <div id="form">
        <form action='<?php echo BASE_URL ?>management/user/search' method="GET">
            <div>
                <label>Email</label><br />
                <input name="email" value="<?php if (isset($_GET['email'])) echo $_GET['email'] ?>" />
            </div>
            <div>
                <label>Name</label><br />
                <input name="name" value="<?php if (isset($_GET['name'])) echo $_GET['name'] ?>" />
            </div>
            <button type="submit">Search</button>
            <a href="<?php echo BASE_URL ?>management/user/userIndex" id="reset">Reset</a>
        </form>
    </div>
</div>

<div class="table">
    <table>
        <thead class='cap'>
            <?php
            if (count($user) > 0) {
                $keys = array_keys($user[0]);
                foreach ($keys as $key) { ?>
                    <th class='cap'>
                        <a href="<?php echo $urlSort ?><?php echo $current_page ?>&column=<?php echo $key  ?>">
                            <?php echo $key;
                            if ($key != 'avatar') {
                            ?>
                                <i class="fa fa-sort" aria-hidden="true"></i>
                        </a>
                    </th>
                <?php } ?>
            <?php  }
            ?>
        </thead>
        <tbody>
            <?php foreach ($user as $item) { ?>
                <tr>
                    <td><?php echo $item['id'] ?></td>
                    <td><?php echo $item['name'] ?></td>
                    <td><?php echo $item['email'] ?></td>
                    <td><?php echo $item['facebook_id'] ?></td>
                    <td><?php echo "<img src= '{$item['avatar']}'/>" ?></td>
                    <td><a href="<?php echo BASE_URL ?>management/user/edit?id=<?php echo $item['id'] ?>">Update</a></td>
                    <td><a href="<?php echo BASE_URL ?>management/user/delete?id=<?php echo $item['id'] ?>" onclick="return confirm('Are you sure')">Delete</a></td>
                </tr>
        <?php }
            } else {
                echo "<p class='notification'>There are no record</p>";
            } ?>
        </tbody>

    </table>
    <?php require_once 'views/layouts/pagination.php' ?>
</div>