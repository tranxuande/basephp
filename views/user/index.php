<link rel="stylesheet" href="assets/css/user/index.css" />
<a href="<?php echo BASE_URL ?>logout">Logout</a>
<div class="container">
    <h1>User Information</h1>
    <form>
        <div>
            <label>Name</label><br />
            <input value="<?php if (isset($_SESSION['user'])) echo $_SESSION['user']['name'];
                            elseif (isset($_SESSION['current_user'])) echo $_SESSION['current_user']['name'] ?>" readonly />
        </div>
        <div>
            <label>Facebook_id</label><br />
            <input value="<?php if (isset($_SESSION['user'])) echo $_SESSION['user']['facebook_id'];
                            elseif (isset($_SESSION['current_user'])) echo $_SESSION['current_user']['facebook_id'] ?>" readonly />
        </div>
        <div>
            <label>Email</label><br />
            <input value="<?php if (isset($_SESSION['user'])) echo $_SESSION['user']['email'];
                            elseif (isset($_SESSION['current_user'])) echo $_SESSION['current_user']['email'] ?>" readonly />
        </div>
        <div>
            <label>Avatar</label><br />
            <img src="<?php if (isset($_SESSION['user'])) echo $_SESSION['user']['avatar'];
                        elseif (isset($_SESSION['current_user'])) echo  $_SESSION['current_user']['avatar'] ?>" height="100px" width="100px" />
        </div>
    </form>
</div>

<!-- https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=1744402982563673&height=50&width=50&ext=1666175384&hash=AeSYsONbP_w6b6EbtKQ -->