<link rel="stylesheet" href="assets/css/user/login.css" />


<div id="container">

    <p><strong>Welcome.</strong>Please login</p>

    <form method="POST" action="./index.php/login">
        <input type="text" placeholder="Email" name="email" /><br />
        <?php echo isset($error['email_blank']) ? "<div class='input'>" . $error['email_blank'] . "</div/>" : '' ?>
        <input type="password" placeholder="Password" name="password" /><br />
        <?php echo isset($error['password_blank']) ? "<div class='input'>" . $error['password_blank'] . "</div/>" : '' ?>
        <?php if (isset($error['account_not_exist'])) echo  "<div class='input'>" . $error['account_not_exist'] . "</div/>" ?>
        <button type="submit">LOGIN</button>
    </form>

    <div id="btn-circle"><span>OR</span></div>

    <a href="<?= $loginUrl ?>">
        LOGIN WITH FACEBOOK
    </a>

</div>