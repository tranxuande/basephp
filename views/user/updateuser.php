<link rel="stylesheet" href="assets/css/admin/style.css" />
<div class="container">
    <h2>Update user</h2>
    <form action="<?php echo BASE_URL ?>management/user/update?id=<?php echo $user['id'] ?>" method="POST" enctype="multipart/form-data">
        <div>
            <label>Name</label><br />
            <input class="in" name="name" value="<?php if (isset($_POST['name'])) echo $_POST['name'];
                                                    else echo isset($user) ? $user['name'] : '' ?>" />
            <?php echo isset($error['name']) ? $error['name'] : '' ?>
        </div>
        <div>
            <label>Password</label><br />
            <input class="in" name="password" type="password" value="<?php if (isset($_POST['password'])) echo $_POST['password'] ?>" />
            <?php echo isset($error['password_format']) ? "<div class='input'>" . $error['password_format'] . "</div>" : '' ?>
        </div>
        <div>
            <label>Password verify</label><br />
            <input name="password_verify" type='password' class="in" />
            <?php if (isset($error['password_verify'])) {
                echo "<div class='input'>" . $error['password_verify'] . "</div>";
            } ?>
        </div>
        <div>
            <label>email</label><br />
            <input class="in" name="email" value="<?php if (isset($_POST['email'])) echo $_POST['email'];
                                                    else echo isset($user) ? $user['email'] : '' ?>" />
            <?php
            if (isset($error['email_blank'])) {
                echo  $error['email_blank'];
            } elseif (isset($error['email_format'])) {
                echo $error['email_format'];
            }
            ?>
        </div>
        <div>
            <label>Avatar</label><br />
            <input name="avatar" type="file" onchange="loadFile(event)" /><br />
            <img src=" <?php if ((isset($_SESSION['img']))) {
                            echo  $_SESSION['img'];
                        } elseif (isset($_SESSION['img_update'])) {
                            echo $_SESSION['img_update'];
                        }

                        ?>" height="100px" width="100px" id="output" />
            <?php
            if (isset($error['file_format'])) {
                echo $error['file_format'];
            }
            ?>
        </div>
        <div>
            <label>Facebook_id</label><br />
            <input class="in" value="<?php echo isset($user) ? $user['facebook_id'] : '' ?>" readonly />
        </div>
        <button type="submit">Update</button>
    </form>
</div>
<a href="<?php echo BASE_URL ?>management/user/userIndex">Back</a>
<script>
    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src)
        }
    };
</script>